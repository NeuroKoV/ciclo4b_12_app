import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
  final dynamic tipoTexto;
  final String label;
  final bool obscure;
  final int maxlineas;

  const MyTextField({
    Key? key,
    this.tipoTexto = TextInputType.text,
    required this.label,
    required this.obscure,
    this.maxlineas = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: tipoTexto,
      cursorColor: Colors.green[600],
      obscureText: obscure,
      decoration: InputDecoration(
        floatingLabelStyle: const TextStyle(color: Color(0xFF43A047)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0xFF43A047)),
            borderRadius: BorderRadius.circular(8)),
        labelText: label,
        contentPadding: const EdgeInsetsDirectional.fromSTEB(20, 20, 0, 20),
      ),
      maxLines: maxlineas,
    );
  }
}
