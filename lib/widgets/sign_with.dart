import 'package:flutter/material.dart';

class SignWith extends StatelessWidget {
  final Widget child;
  final String text;

  const SignWith({
    Key? key,
    required this.text,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      margin: const EdgeInsets.symmetric(horizontal: 12.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          child,
          Text(
            text,
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 16, color: Color(0xFF43A047)),
          )
        ],
      ),
    );
  }
}
