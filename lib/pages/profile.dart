import 'package:flutter/material.dart';
import 'package:freshfarm/pages/edit_profile.dart';
import 'package:freshfarm/pages/home.dart';
import 'package:freshfarm/pages/login.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.green[600],
        automaticallyImplyLeading: false,
        leading: _back(),
        title: const Text(
          'Perfil',
          style: TextStyle(
            fontFamily: 'Poppins',
            color: Colors.white,
            fontSize: 25,
          ),
        ),
        // actions: [],
        centerTitle: true,
        elevation: 0,
      ),
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _profileDates(),
                  const Text(
                    'Account',
                  ),
                  _farmerBtn(),
                  _payBtn(),
                  _countryBtn(),
                  _notificationBtn(),
                  _profileBtn(),
                  const Text(
                    'General',
                  ),
                  _suportBtn(),
                  _serviceBtn(),
                  _shareBtn(),
                  Container(
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                          blurRadius: 5,
                          color: Color(0x3416202A),
                          offset: Offset(0, 2),
                        )
                      ],
                      borderRadius: BorderRadius.circular(12),
                      shape: BoxShape.rectangle,
                    ),
                    child: ElevatedButton(
                      onPressed: () {}, //() async {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) => const LoginWidget(),
                      //   ),
                      // );
                      // },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green[600],
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 15),
                          textStyle: const TextStyle(
                            fontSize: 20,
                          )),
                      child: const Text("Cerrar sesion"),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _back() {
    return IconButton(
      icon: const Icon(
        Icons.arrow_back_rounded,
        color: Colors.white,
        size: 30,
      ),
      onPressed: () async {
        Navigator.pop(context);
      },
    );
  }

  Widget _profileDates() {
    const user = 'Diego Carrasco';
    const email = 'diego@correo.com';

    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(0, 1, 0, 0),
      child: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              blurRadius: 0,
              color: Colors.white,
              offset: Offset(0, 1),
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(16, 8, 16, 8),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Card(
                clipBehavior: Clip.antiAliasWithSaveLayer,
                color: Colors.white,
                elevation: 2,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(2, 2, 2, 2),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: Image.asset(
                      'assets/farm.png',
                      width: 60,
                      height: 60,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(16, 0, 0, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      user,
                    ),
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(0, 4, 0, 0),
                      child: Text(
                        email,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _farmerBtn() {
    return mySelection(
      Icons.agriculture,
      'Farmer',
      const HomePage(),
    );
  }

  Widget _payBtn() {
    return mySelection(
      Icons.attach_money,
      'Opciones de Pago',
      const HomePage(),
    );
  }

  Widget _countryBtn() {
    return mySelection(
      Icons.language_outlined,
      'Seleccine Pais',
      const EditProfilePage(),
    );
  }

  Widget _notificationBtn() {
    return mySelection(
      Icons.notifications_none,
      'Notificacionesasd',
      const LoginPage(),
    );
  }

  Widget _profileBtn() {
    return mySelection(
      Icons.account_circle_outlined,
      'Editar Perfil',
      const EditProfilePage(),
    );
  }

  Widget _suportBtn() {
    return mySelection(
      Icons.help_outline_rounded,
      'Soporte',
      const HomePage(),
    );
  }

  Widget _serviceBtn() {
    return mySelection(
      Icons.privacy_tip_rounded,
      'Terminos del Servicio',
      const HomePage(),
    );
  }

  Widget _shareBtn() {
    return mySelection(
      Icons.ios_share,
      'Compartir con Amigos',
      const HomePage(),
    );
  }

  Widget mySelection(icono, String nameText, dynamic pagina) {
    return Container(
      margin: const EdgeInsets.only(top: 8, bottom: 8),
      width: double.infinity,
      height: 60,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            blurRadius: 5,
            color: Colors.green,
            offset: Offset(0, 2),
          )
        ],
        borderRadius: BorderRadius.circular(12),
        shape: BoxShape.rectangle,
      ),
      child: Padding(
        padding: const EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
        child: InkWell(
          onTap: () async {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => pagina,
              ),
            );
          },
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Icon(
                icono,
                color: Colors.blueGrey,
                size: 24,
              ),
              Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(12, 0, 0, 0),
                child: Text(
                  nameText,
                ),
              ),
              const Expanded(
                child: Align(
                  alignment: AlignmentDirectional(0.9, 0),
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.blueGrey,
                    size: 18,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
