import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({super.key});

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(100),
        child: AppBar(
          backgroundColor: Colors.green[600],
          automaticallyImplyLeading: false,
          title: Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 20, 0, 14),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 400,
                  height: 100,
                  decoration: const BoxDecoration(
                    shape: BoxShape.rectangle,
                  ),
                  child: Padding(
                    padding: const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsetsDirectional.fromSTEB(5, 6, 0, 0),
                          child: IconButton(
                            icon: const Icon(
                              Icons.arrow_back_rounded,
                              color: Colors.white,
                              size: 30,
                            ),
                            onPressed: () async {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsetsDirectional.fromSTEB(30, 6, 0, 0),
                          child: Text(
                            'Editar Perfil',
                            style: TextStyle(
                              fontFamily: 'Lexend Deca',
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: const [],
          flexibleSpace: FlexibleSpaceBar(
            background: Image.asset(
              'assets/watercolorv.png',
              fit: BoxFit.cover,
            ),
          ),
          centerTitle: true,
          elevation: 0,
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(20, 20, 20, 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _profileImage(),
                Padding(
                    padding: const EdgeInsetsDirectional.fromSTEB(0, 20, 0, 10),
                    child: SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                            onPressed: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) => const ProfileWidget(),
                              //   ),
                              // );
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.green[600],
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 15),
                                textStyle: const TextStyle(fontSize: 20)),
                            child: const Text('Cambiar Foto')))),
                _formEditProfile(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _profileImage() {
    const user = 'Diego Carrasco';
    const email = 'diego@correo.com';

    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.all(8.0),
      child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white60,
                  width: 5,
                  strokeAlign: StrokeAlign.inside,
                ),
                color: Colors.green,
                shape: BoxShape.circle,
              ),
              child: Container(
                width: 90,
                height: 90,
                clipBehavior: Clip.antiAlias,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  'assets/farm.png',
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.all(4.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text(
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 16,
                    ),
                    user,
                  ),
                  Text(
                    email,
                    style: TextStyle(
                      fontSize: 14,
                      height: 1.5,
                    ),
                  ),
                ],
              ),
            ),
          ]),
    );
  }

  Widget _formEditProfile(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _userName(),
          const SizedBox(height: 20),
          _userLastName(),
          const SizedBox(height: 20),
          _userEmail(),
          const SizedBox(height: 20),
          _userAbout(),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 10, 0, 10),
            child: SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.green[600],
                    padding: const EdgeInsets.symmetric(
                        horizontal: 30, vertical: 15),
                    textStyle: const TextStyle(fontSize: 20)),
                child: const Text('Guardar'),
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    //TODO: Guardar datos en BD.
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text("Datos Almacenados con exito"),
                      ),
                    );
                    Navigator.pop(context);
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  TextFormField _userName() {
    return TextFormField(
      cursorColor: Colors.green,
      autofocus: true,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        labelText: "Nombres",
        floatingLabelStyle: TextStyle(color: Colors.green),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green, width: 2.0),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
            borderRadius: BorderRadius.all(Radius.circular(8))),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Campo obligatorio";
        }
        return null;
      },
    );
  }

  TextFormField _userLastName() {
    return TextFormField(
      cursorColor: Colors.green,
      autofocus: false,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        labelText: "Apellidos",
        floatingLabelStyle: TextStyle(color: Colors.green),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green, width: 2.0),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
            borderRadius: BorderRadius.all(Radius.circular(8))),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Campo obligatorio";
        }
        return null;
      },
    );
  }

  TextFormField _userEmail() {
    return TextFormField(
      cursorColor: Colors.green,
      autofocus: false,
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
        labelText: "Correo Electrónico",
        floatingLabelStyle: TextStyle(color: Colors.green),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green, width: 2.0),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
            borderRadius: BorderRadius.all(Radius.circular(8))),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Campo obligatorio";
        }
        return null;
      },
    );
  }

  TextFormField _userAbout() {
    return TextFormField(
      maxLines: 5,
      // expands: true,
      cursorColor: Colors.green,
      autofocus: false,
      keyboardType: TextInputType.text,
      decoration: const InputDecoration(
        labelText: "Sobre ti...",
        floatingLabelStyle: TextStyle(color: Colors.green),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green, width: 2.0),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
            borderRadius: BorderRadius.all(Radius.circular(8))),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Campo obligatorio...";
        }
        return null;
      },
    );
  }
}
