import 'package:flutter/material.dart';
import 'package:freshfarm/pages/home.dart';

import '../widgets/sign_with.dart';

class LoginPage extends StatelessWidget {
  final _imageLogin = "assets/images/banner.jpg";

  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.green[50],
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Image.asset(_imageLogin),
            Padding(
              padding: const EdgeInsets.all(14.0),
              child: Column(
                children: <Widget>[
                  _formulario(context),
                  _olvidoCont(),
                  const Divider(
                    height: 20,
                    thickness: 2,
                    // indent: 20,
                    // endIndent: 20,
                    color: Color(0xFF43A047),
                  ),
                  _inicioAlternativo(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _campoUsuario(),
          const SizedBox(height: 20),
          _campoCont(),
          const SizedBox(height: 20),
          SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      //TODO: validar uduario y contraseña en BD

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const HomePage(),
                          ));
                    }
                  },
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.green[600],
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 15),
                      textStyle: const TextStyle(fontSize: 20)),
                  child: const Text('Inicar Sesión'))),
        ],
      ),
    );
  }

  _campoUsuario() {
    return TextFormField(
        maxLength: 50,
        keyboardType: TextInputType.emailAddress,
        cursorColor: Colors.green[600],
        decoration: InputDecoration(
          floatingLabelStyle: const TextStyle(color: Color(0xFF43A047)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
          focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Color(0xFF43A047)),
              borderRadius: BorderRadius.circular(8)),
          labelText: "Usuario",
          contentPadding: const EdgeInsetsDirectional.fromSTEB(20, 20, 0, 20),
        ),
        validator: (val) {
          if (val == null || val.isEmpty) {
            return "El Campo debe contener Correo Electrónico";
          }
          if (!val.contains('@') || !val.contains('.')) {
            return "Ingrese Correo Electrónico";
          }
          return null;
        });
  }

  _campoCont() {
    return TextFormField(
      maxLength: 16,
      cursorColor: Colors.green[600],
      obscureText: true,
      decoration: InputDecoration(
        floatingLabelStyle: const TextStyle(color: Color(0xFF43A047)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0xFF43A047)),
            borderRadius: BorderRadius.circular(8)),
        labelText: "Contraseña",
        contentPadding: const EdgeInsetsDirectional.fromSTEB(20, 20, 0, 20),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Contraseña Requerida";
        }
        if (value.length < 8) {
          return "La contraseña debe tener más de 8 caracteres";
        }
        return null;
      },
    );
  }

  Widget _inicioAlternativo() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SignWith(
            text: 'Gmail',
            child: IconButton(
                onPressed: () {},
                icon: const Icon(Icons.email),
                iconSize: 30,
                color: Colors.green[600])),
        const SizedBox(
          width: 30,
        ),
        SignWith(
            text: 'Facebook',
            child: IconButton(
              onPressed: () {},
              icon: const Icon(Icons.facebook),
              iconSize: 30,
              color: Colors.green[600],
            )),
        const SizedBox(
          width: 30,
        ),
        SignWith(
            text: 'Registrese',
            child: IconButton(
              onPressed: () {},
              icon: const Icon(Icons.app_registration_outlined),
              iconSize: 30,
              color: Colors.green[600],
            ))
      ],
    );
  }

  Widget _olvidoCont() {
    return TextButton(
      style: TextButton.styleFrom(
        foregroundColor: Colors.green[600],
        textStyle: const TextStyle(
          fontSize: 15,
        ),
      ),
      onPressed: () {},
      child: const Text('¿Olvidó su contraseña?'),
    );
  }
}
