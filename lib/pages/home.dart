import 'package:flutter/material.dart';
import 'package:freshfarm/pages/profile.dart';

import '../widgets/card_categories.dart';
import '../widgets/cards_products.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    const user = "Diego";

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[600],
        // automaticallyImplyLeading: false,
        leading: InkWell(
          onTap: () => {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const ProfilePage(),
                ))
          },
          child: const Icon(
            Icons.person,
            color: Colors.white,
            size: 50,
          ),
        ),
        title: const Text(
          'Bienvenido, $user',
          textAlign: TextAlign.start,
          style: TextStyle(
            fontFamily: 'Outfit',
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.normal,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 0, 12, 0),
            child: IconButton(
              icon: const Icon(
                Icons.shopping_cart_outlined,
                color: Colors.white,
                size: 30,
              ),
              onPressed: () {},
            ),
          ),
        ],
        centerTitle: false,
        elevation: 0,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.infinity,
                height: 80,
                decoration: const BoxDecoration(
                  color: Color(0xFF43A047),
                ),
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(16, 12, 16, 0),
                  child: TextFormField(
                    obscureText: false,
                    decoration: InputDecoration(
                      labelText: 'Buscar productos....',
                      labelStyle: const TextStyle(
                        color: Colors.grey,
                        fontSize: 16,
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: Color(0xFF43A047),
                            width: 2,
                          ),
                          borderRadius: BorderRadius.circular(12)),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color(0xFF43A047),
                          width: 2,
                        ),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      prefixIcon: const Icon(
                        Icons.search_rounded,
                        color: Colors.grey,
                      ),
                    ),
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
              _myViewList(context),
              Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(16, 4, 16, 0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(0, 4, 0, 4),
                      child: Text(
                        'Categorias',
                        style: TextStyle(
                          fontFamily: 'Outfit',
                          color: Colors.blueGrey,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Text(
                      'Ver Todo',
                      style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(0, 12, 0, 44),
                child: Wrap(
                  spacing: 8,
                  runSpacing: 8,
                  alignment: WrapAlignment.start,
                  crossAxisAlignment: WrapCrossAlignment.start,
                  direction: Axis.horizontal,
                  runAlignment: WrapAlignment.start,
                  verticalDirection: VerticalDirection.down,
                  clipBehavior: Clip.none,
                  children: const [
                    CategoriesCard(
                      enlace:
                          'https://static.abc.es/media/MM/2019/12/30/lechuga-kv5D--1350x900@abc.jpg',
                      titulo: 'Lechuga',
                      descripcion: 'Category Name',
                    ),
                    CategoriesCard(
                      enlace:
                          'https://huerto-en-casa.com/wp-content/uploads/2020/10/Zanahoria-es-fruta-o-verdura.jpg',
                      titulo: 'Zanahoria',
                      descripcion: 'Category Name',
                    ),
                    CategoriesCard(
                      enlace:
                          'https://www.gob.mx/cms/uploads/article/main_image/16714/cebolla.jpg',
                      titulo: 'Cebolla',
                      descripcion: 'Category Name',
                    ),
                    CategoriesCard(
                      enlace:
                          'https://images-prod.healthline.com/hlcmsresource/images/AN_images/tomatoes-1296x728-feature.jpg',
                      titulo: 'Tomate',
                      descripcion: 'Category Name',
                    ),
                    CategoriesCard(
                      enlace:
                          'https://pagina66.com/upload/images/05_2021/7860_apio.jpg',
                      titulo: 'Apio',
                      descripcion: 'Category Name',
                    ),
                    CategoriesCard(
                      enlace:
                          'https://www.ecoticias.com/wp-content/uploads/2022/02/zbrocoli.jpg',
                      titulo: 'Brócoli',
                      descripcion: 'Category Name',
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _myViewList(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 110,
      decoration: const BoxDecoration(),
      child: Padding(
        padding: const EdgeInsetsDirectional.fromSTEB(1, 1, 1, 1),
        child: ListView(
          padding: EdgeInsets.zero,
          primary: false,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          children: const [
            ProductsCards(
              texto: 'Verduras',
              icono: Icons.grass,
            ),
            ProductsCards(
              texto: 'Frutas',
              icono: Icons.apple,
            ),
            ProductsCards(
              texto: 'Lácteos',
              icono: Icons.coffee,
            ),
            ProductsCards(
              texto: 'Carnes',
              icono: Icons.heat_pump_outlined,
            ),
          ],
        ),
      ),
    );
  }
}
